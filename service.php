<?php
include 'config.php';

try
{
    $host=$config['DB_HOST'];
    $dbname=$config['DB_DATABASE'];
    $dbh = new PDO("mysql:host=$host;dbname=$dbname",$config['DB_USERNAME'],$config['DB_PASSWORD']);
}
catch(PDOException $e)
{
    echo "Error:".$e->getMessage();
}

function setData()
{
    global $dbh;
    global $config;
    $token = $config['TOKEN'];;

    ini_set('max_execution_time', 900);
    $i = 0;
    $sql = "INSERT INTO projects (
                  name_project,
                  budget,
                  currency,
                  link_project,
                  name_employer,
                  login_employer,
                  status,
                  skill_name,
                  skill_id,
                  project_id
                  ) VALUES (
                  :name_project,
                  :budget,
                  :currency,
                  :link_project,
                  :name_employer,
                  :login_employer,
                  :status,
                  :skill_name,
                  :skill_id,
                  :project_id
                  )";
    $stmt = $dbh->prepare($sql);

    do {
        $url = "https://api.freelancehunt.com/v2/projects?page[number]=" . ($i+1);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$token,
                "Content-Type: application/json",

            ),
            CURLOPT_SSL_VERIFYPEER => false,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true);

        if (isset($response["data"])) {
            foreach ($response["data"] as $row) {
                $data = [
                    'name_project' => $row["attributes"]["name"] ?? null,
                    'budget' => $row["attributes"]["budget"]["amount"] ?? null,
                    'currency' => $row["attributes"]["budget"]["currency"] ?? null,
                    'link_project' => $row["links"]["self"]["web"] ?? null,
                    'name_employer' => $row["attributes"]["employer"]["first_name"] ?? null,
                    'login_employer' => $row["attributes"]["employer"]["login"] ?? null,
                    'status' => $row["attributes"]["status"]["name"] ?? null,
                    'skill_name' => $row["attributes"]["skills"][0]["name"] ?? null,
                    'skill_id' => $row["attributes"]["skills"][0]["id"] ?? null,
                    'project_id' => $row["id"],
                ];
                $stmt->execute($data);
            }
        } else {
            break;
        }
        $i++;
    } while (true);
    return $i;
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>TEST</title>
    <meta charset="utf-8">
</head>
<body>
    <?php
    $n = setData();
    if($n>0): ?>
        <h1 align="center" style="margin-top: 50px">Импортировал <?= $n ?> страниц</h1>
        <h3 align="center">Для отображений даных кликните <a href="/">СЮДА</a></h3>
    <?php else: ?>
        <h1 align="center" style="margin-top: 50px; color: red">что то пошло не так(</h1>
    <?php endif; ?>
</body>
</html>
