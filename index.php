<?php

include 'config.php';

try
{
    $host=$config['DB_HOST'];
    $dbname=$config['DB_DATABASE'];
    $dbh = new PDO("mysql:host=$host;dbname=$dbname",$config['DB_USERNAME'],$config['DB_PASSWORD']);
}
catch(PDOException $e)
{
    echo "Error:".$e->getMessage();
}

$urlBank = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
$curlBank = curl_init();

curl_setopt_array($curlBank, array(
    CURLOPT_URL => $urlBank,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
    ),
    CURLOPT_SSL_VERIFYPEER => false,
));

$response = curl_exec($curlBank);
curl_close($curlBank);

$responseBank = json_decode($response, true);

function initData(){
    global $dbh;
    $projects = $dbh->prepare("SELECT * FROM projects");
    $projects->execute();

    if ($projects->rowCount() > 0) {
        getData();
    } else {
        setData();
    }
}

function getExchange($currency = '', $sum = 0){
    global $responseBank;

    switch ($currency){
        case 'USD':
            $curs = $responseBank[0]["sale"];
            break;
        case 'EUR':
            $curs = $responseBank[1]["sale"];
            break;
        case 'RUB':
            $curs = $responseBank[2]["sale"];
            break;
        default: $curs = 1;
    }
    return intval(round($curs*$sum));
}

function setBudget(){
    global $dbh;
    $projects = $dbh->prepare("SELECT * FROM projects");
    $projects->execute();
    $budgetData=[];

    while ($row = $projects->fetch(PDO::FETCH_ASSOC)) {
        $budgetData[] = getExchange($row["currency"], $row["budget"]);
    }
    return $budgetData;
}

function getData(){
    global $dbh;

    if (!isset($_GET['page'])) {
        $page = 1;
    } else{
        $page = $_GET['page'];
    }

    $limit = 10;
    $starting_limit = ($page-1)*$limit;

    $category = [1,99,86];
    $qMarks = str_repeat('?,', count($category) - 1) . '?';
    $getProjectsPerPage = $dbh->prepare("SELECT * FROM projects WHERE skill_id IN ($qMarks) limit " . $starting_limit . ",". $limit);

    $getProjectsPerPage->execute($category);
    $projects = $getProjectsPerPage->fetchAll();

    $getProjects = $dbh->prepare("SELECT * FROM projects WHERE skill_id IN ($qMarks)");
    $getProjects->execute($category);
    $total_results = count($getProjects->fetchAll());

    $total_pages = ceil($total_results/$limit);

    showData($projects);
    for ($page=1; $page <= $total_pages ; $page++){
        echo "<a href='?page=".$page."' style='display: inline-block; margin: 10px'>".$page."</a>";
        }
}

function budget500($var){
    return $var < 500;
}
function budget1000($var){
    return $var >= 500 && $var < 1000;
}
function budget5000($var){
    return $var >= 1000 && $var < 5000;
}
function budgetAfter5000($var){
    return $var >= 5000;
}
function getDataForChart(){
  $amountData = setBudget();
  return [
    'до 500 грн' => count(array_filter($amountData, "budget500")),
    '500-1000 грн' => count(array_filter($amountData, "budget1000")),
    '1000-5000 грн' => count(array_filter($amountData, "budget5000")),
    'более 5000 грн' => count(array_filter($amountData, "budgetAfter5000"))
  ];
}
?>

<?php function showData($projects) { ?>
    <?php if (count($projects) > 0): ?>
        <table border="1" width="100%" cellpadding="5">
            <thead>
            <tr>
                <th>№</th>
                <th>Название проекта</th>
                <th>Категория</th>
                <th>Бюджет</th>
                <th>Имя заказчика</th>
                <th>Логин заказчика</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($projects as $row): ?>
                <tr>
                    <td><?php echo $row["id"] ?></td>
                    <td><a href="<?php echo $row["link_project"] ?>"><?php echo $row["name_project"] ?></a></td>
                    <td><?php echo $row["skill_name"] ?></td>
                    <td><?php echo $row["budget"];  echo $row["currency"]?></td>
                    <td><?php echo $row["name_employer"] ?></td>
                    <td><?php echo $row["login_employer"] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
<?php } ?>

<!DOCTYPE html>
<html>
    <head>
        <title>TEST</title>
        <meta charset="utf-8">
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
          window.onload = function () {
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawchart);
            function drawchart() {
              var data = google.visualization.arrayToDataTable([
                ['budget','id'],
                  <?php
                  foreach (getDataForChart() as $key=>$value)
                      echo "['".$key."', ".$value."],";
                  ?>
              ]);
              var options = {
                title: "Бюджеты проектов"
              };
              var chart = new google.visualization.PieChart(document.getElementById('piechart'));
              chart.draw(data, options);
            }
          };
          </script>
    </head>
    <body>
        <?php initData(); ?>
    <div id="piechart" style="width: 900px; height: 500px"></div>
    </body>
</html>
